from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render
from django.core.mail import send_mail
from django.utils.safestring import mark_safe
from blog.Calendar import WorkoutCalendar
import datetime, json
from django.template.loader import render_to_string

def index(request):
	params = {
		'title': 'Store'
	}
	return render_to_response('istore-index.html', params)
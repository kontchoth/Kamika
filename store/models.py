from __future__ import unicode_literals
from .store_constants import *
from django.db import models
from datetime import datetime
import django


class ProductImage(models.Model):
	image = models.ImageField(upload_to='product-images', blank=False)
	description = models.TextField(blank=False, default='No Description')
	created_at = models.DateTimeField(auto_now=True)

class ProductCategorie(models.Model):
	name = models.CharField(max_length=100, default='ALL', unique=True)
	created_at = models.DateTimeField(auto_now=True)


class Product(models.Model):
	category = models.ForeignKey(ProductCategorie, blank=False)
	name = models.CharField(max_length=200, blank=False)
	description = models.TextField(blank=False, default='No Description')
	images = models.ManyToManyField(ProductImage, blank=True)

class Stock(models.Model):
	product = models.ForeignKey(Product)
	quantity = models.IntegerField(blank=False, default=0)
	size = models.CharField(max_length=1, choices=PRODUCT_SIZES)
	price = models.FloatField(blank=False, default=0)


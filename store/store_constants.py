PRODUCT_SIZES = (
    ('S', 'Small'),
    ('M', 'Medium'),
    ('L', 'Large'),
)

PRODUCT_STATUS = (
	('A', 'Available'),
	('O', 'Out of Stock')
)


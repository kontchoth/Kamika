from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render
from django.core.mail import send_mail
from django.utils.safestring import mark_safe
from blog.Calendar import WorkoutCalendar
import datetime
from django.template import Template, Context
import json

def index(request):
	params = {
		'title': 'Homepage'
	}
	return render_to_response('iressources-index.html', params)

def agencies(request):
	params = {
		'title': 'Agencies'
	}
	template = Template('iresources-agencies.html')
	template = template.render()
	print template
	return HttpResponse(json.dumps({'status':200, 'html':template}), content_type='application/json')

def agencies_information(request, state):
	params = {
		'title': 'Agencies',
		'state': state
	}
	return render_to_response('iresources-agencies-md.html', params)
from django.conf.urls import url, include
from django.contrib import admin
from . import views


urlpatterns = [
    url(r'^$', views.index),
    url(r'^agencies$', views.agencies),
    url(r'^agencies/(?P<state>\w{2,40})/$', views.agencies_information),

    
]

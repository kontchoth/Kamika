from django.contrib import admin
from blog.models import Blog, Comment, Category, BlogImage

class CategoryAdmin(admin.ModelAdmin):
	fieldsets = (
		('Image Information', {
			'fields': ('name', 'active', 'created_at')
		}),
	)
	readonly_fields = ('created_at', )
	list_display = ('name', 'active', 'created_at')

class BlogAdmin(admin.ModelAdmin):
	fieldsets = (
		('General Information', {
			'fields': ('category', 'name', 'description', 'content', 'author', 'views','commentable'),
		}),
		('Blog Images', {
			'fields': ('images',),
		}),
	)
	readonly_fields = ('created_at', 'updated_at', 'views')
	list_display = ('category', 'name', 'author', 'commentable', 'display_images', 'views')

class BlogImageAdmin(admin.ModelAdmin):
    fields = ('image', 'description')
    readonly_fields = ('created_at', 'updated_at')

class CommentAdmin(admin.ModelAdmin):
    fields = ('author', 'content', 'is_active')
    readonly_fields = ('email', 'posted', 'blog', 'content', 'author')
    list_display = ('author','blog', 'content')
# Register your models here.
admin.site.register(Category, CategoryAdmin)
admin.site.register(Blog, BlogAdmin)
admin.site.register(BlogImage, BlogImageAdmin)
admin.site.register(Comment, CommentAdmin)

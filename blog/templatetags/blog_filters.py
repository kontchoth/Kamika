from django import template


register = template.Library()

@register.filter(name='get_blog_display_image')
def get_blog_display_image(images):
  if len(images):
    return images[0].image.url
  return 'http://placehold.it/350x150'

@register.filter(name='short_description')
def short_description(description, key):
  if key:
    if len(description) > 200:
      return description[:200] + '...'
  return description
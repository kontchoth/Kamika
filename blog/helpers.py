from blog.models import Blog, BlogImage, Category, Comment
from django.core.exceptions import *
from Calendar import WorkoutCalendar
def getAllBlogs():
	'''
	This function get all the blogs on the DB and along with their
	appropriate image and return then as a list.

	Return an empty list if DB is empty
	'''
	blogs = []
	all_blogs = Blog.objects.all().order_by('-updated_at')

	for blog in all_blogs:
	    blogs.append({
	        'blog': blog,
	        'images': BlogImage.objects.all().filter(blog=blog)
	    });
	return blogs

def getBlogByCategory(cat):
	'''
	This function returns all blogs on the system of a specific
	categories and returns them as a list.

	return empty list is nothing is found
	'''
	blogs = []
	try:
	    category = Category.objects.get(name__iexact=cat)
	    if category:
	        all_blogs = Blog.objects.all().filter(category=category).order_by('-updated_at')
	        for blog in all_blogs:
	            blogs.append({
	                'blog': blog,
	                'images': BlogImage.objects.all().filter(blog=blog)
	            });
	    else:
	        category = cat
	except ObjectDoesNotExist:
	    category = cat
	    blogs = []
	return blogs, category

def getSingleBlog(cat, title, blog_only=True):
	my_blog = None
	try:
		category = Category.objects.get(name__iexact=cat)
		if category:
			blog = Blog.objects.get(category=category, name__iexact=title)
			if blog:
				comments = []
				if not blog_only:
					comments = Comment.objects.all().filter(blog=blog, is_active=True)
				my_blog = {
					'blog': blog,
					'images': BlogImage.objects.all().filter(blog=blog),
					'comments': comments,
					'num_comments': len(comments)
				}
	except Exception as e:
		blog = None
	return my_blog

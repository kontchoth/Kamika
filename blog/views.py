from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render
from django.template.loader import render_to_string
from django.core.mail import send_mail
from helpers import getAllBlogs, getSingleBlog
from models import Comment
import json

def index(request):
	params = {
		'title': 'Blogs',
		'blogs': getAllBlogs(),
    'single': False,
    'sm_version': True,
    'show_readmore': True,
    'show_comments': False
	}

	return render_to_response('icoach-blogs-index.html', params)

def blog_index(request, category, title):
  params = {
    'title': title.replace('-', ' '),
    'blog': getSingleBlog(category, title.replace('-', ' '), blog_only=False),
    'single': True,
    'sm_version': False,
    'show_readmore': False,
    'show_comments': True
  }
  return render_to_response('icoach-blogs-index.html', params, RequestContext(request))

def post_comment(request):
  if request.method == 'POST':
    author = request.POST['author']
    email = request.POST['email']
    website = request.POST['website']
    comment = request.POST['comment']
    blogname = request.POST['blogname']
    follow_up = request.POST['follow_up']
    blog_subs = request.POST['blog_subs']
    category = request.POST['category']
    blog = getSingleBlog(category, blogname)
    if blog:
      blog = blog['blog']
    cmt = Comment(blog=blog, author=author, email=email,website=website, content=comment)
    cmt.save()
    html = render_to_string('partials/blog-comment-template.html', {'comment': cmt})
    return HttpResponse(json.dumps({'html':html}), content_type='application/json')
  else:
    return HttpResponse(json.dumps({'html':None}), content_type='application/json')

 
from django.conf.urls import url
from django.contrib import admin
from blog import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^post-comment/$', views.post_comment),
    url(r'^(?P<category>[\w|-]+)/(?P<title>[\w|-]+)/$', views.blog_index)
]

from django.db import models
from datetime import datetime
import time
import django
from tinymce.models import HTMLField

class Category(models.Model):
    '''
    Category model is use to group the Blog in category.
    It content only 4 fields
       1. name(CharField) -- name of the category
       2. active (Boolean) -- define weither or not the category is active
       3. created_at(datetime) -- record the data when it was created
       4. frequency (integer) -- keep track of the number of blog of this type
    '''
    name = models.CharField(max_length=100, blank=False, unique=True)
    active = models.BooleanField(default=True, blank=False)
    created_at = models.DateTimeField(editable=False, default=django.utils.timezone.now)

    def __str__(self):
        return self.name

    def get_name(self):
        return self.name

    def is_active(self):
        return self.active == True

    def get_created_date(self):
        return self.created_at

class BlogImage(models.Model):
    '''
    This table record all the picture of the Blog system.
    It has 2 fields
    image --- image
    description --- description of the image
    '''
    image = models.ImageField(upload_to='blog-images', blank=False)
    description = models.TextField(blank=False)
    created_at = models.DateTimeField(auto_now=True)
    updated_at = models.DateTimeField(auto_now_add=True)
    

    def __str__(self):
        return self.image.name.split('/')[-1]

    def get_description(self):
        return self.description

    def get_image(self):
        return self.image.name

    def save(self):
        if self.id:
            self.updated_at = datetime.now()
        else:
            self.created_at = datetime.now()
            self.updated_at = self.created_at
        super(BlogImage, self).save()

    def get_updated_date(self):
        return self.updated_at

    def set_description(self, description):
        if description:
            self.description = description
            self.save()
    def image_thumb(self):
        return u'<img src="{}" width="50" height="auto" />'.format(self.image.url) 

class Blog(models.Model):
    '''
    Blog model
    '''
    name = models.CharField(max_length=100, blank=False, unique=True)
    description = models.CharField(blank=False, max_length=150, default='')
    content = HTMLField(blank=False, default='Comming Soon')
    category = models.ForeignKey(Category, blank=False)
    created_at = models.DateTimeField(default=django.utils.timezone.now)
    updated_at = models.DateTimeField(default=django.utils.timezone.now)
    commentable = models.BooleanField(default=True, blank=False)
    author = models.CharField(max_length=100, blank=False, default='KaMyka')
    images = models.ManyToManyField(BlogImage, blank=True)
    views = models.IntegerField(default=0, blank=False)

    def __str__(self):
        return self.name

    def save(self):
        if self.id:
            self.updated_at = datetime.now()
        else:
            self.created_at = datetime.now()
            self.updated_at = self.created_at
        super(Blog, self).save()

    def get_description(self):
        return self.description[:150] + '...'

    def get_content(self):
        '''Return the content of the blog.'''
        return this.content

    def set_description(self, description):
        if description:
            self.description = description
            self.save()

    def get_created_date(self):
        return self.created_at

    def get_updated_date(self):
        return self.updated_at

    def get_author(self):
        return self.author

    def get_blog_url(self):
        return self.name.replace(' ', '-')

    def get_display_image(self):
    	if len(self.images.all()):
    		return self.images.all()[0].image
    	return None
    def display_images(self):
        if len(self.images.all()):
            return ' '.join([image.image_thumb() for image in self.images.all()])
    display_images.short_description = 'Blog Images'
    display_images.allow_tags = True 

class Comment(models.Model):
    blog = models.ForeignKey('Blog', blank=False)
    author = models.CharField(blank=False, max_length=200)
    email = models.EmailField(blank=False, max_length=200)
    website = models.CharField(blank=True, default='', max_length=200)
    content = models.TextField(blank=False)
    posted = models.DateTimeField(editable=False, default=django.utils.timezone.now)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.author


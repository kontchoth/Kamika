from .models import Album, AlbumImage

def get_all_albums():
	all_albums = Album.objects.all()
	albums = []

	for album in all_albums:
		photos = album.photos.all()
		if len(photos):
			photos = photos[0]
		albums.append({
			'title': album.name,
			'front_page': photos.image.url
			}
		)
	return albums

def get_album_photos(album_name):
	album = Album.objects.get(name__iexact=album_name)
	photos = []
	if album:
		for photo in album.photos.all():
			photos.append(photo.image.url)
	return photos
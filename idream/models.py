from django.db import models
from datetime import datetime
import django
# Create your models here.

class AlbumImage(models.Model):
	image = models.ImageField(upload_to='albums', blank=False)
	upload_date = models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.image.name.split('/')[-1]

	def image_thumb(self):
		return u'<img src="{}" width="50" height="auto" />'.format(self.image.url)
	image_thumb.short_description = 'Photo'
	image_thumb.allow_tags = True

class Album(models.Model):
	name = models.CharField(max_length=200, blank=False, unique=True)
	active = models.BooleanField(default=True)
	created_at = models.DateTimeField(editable=False, default=django.utils.timezone.now)
	photos = models.ManyToManyField(AlbumImage, blank=True)
	last_updated = models.DateTimeField(auto_now_add=True)

	def __str__(self):
		return self.name

	def save(self):
		if self.id:
			self.last_updated = datetime.now()
		super(Album, self).save()

	def display_photos(self):
		if len(self.photos.all()):
			return ' '.join([photo.image_thumb() for photo in self.photos.all()])
	display_photos.short_description = 'Album Images'
	display_photos.allow_tags = True


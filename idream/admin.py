from django.contrib import admin
from idream.models import AlbumImage, Album


class AlbumImageAdmin(admin.ModelAdmin):
	fields = ('image',)
	readonly_fields = ('upload_date',)
	list_display = ('upload_date', 'image_thumb',)

class AlbumAdmin(admin.ModelAdmin):
	fieldsets = (
		('General Information', {
			'fields':('name', 'active',),
		}),
		('Album Pictures', {
			'fields':('photos',),
		}),
	)
	readonly_fields = ('created_at', 'last_updated')
	list_display = ('name', 'created_at', 'last_updated', 'display_photos')

admin.site.register(AlbumImage, AlbumImageAdmin)
admin.site.register(Album, AlbumAdmin)

from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render, redirect
from django.core.mail import send_mail
from .models import Album
from .helpers import get_all_albums, get_album_photos
import json

def index(request):
	params = {
		'title': 'Homepage'
	}
	return render_to_response('idream.index.html', params)

def academic(request):
	params = {
		'title':'Academic'
	}
	return render_to_response('idream.academic.html', params)

def project_dream(request):
  params = {
    'title':'Project Dream'
  }
  return render_to_response('idream.project.dream.html', params)

def sponsor(request):
  params = {
    'title':'Become a Sponsor'
  }
  return render_to_response('idream.sponsor.html', params)

def get_album_images(request):
    album = request.GET.get('album', '')
    photos = get_album_photos(album)
    return HttpResponse(json.dumps(photos), content_type='application/json')


def dream_media(request):
  params = {
    'title':'Dream Media',
    'albums': get_all_albums()
  }
  return render_to_response('idream.media.html', params)

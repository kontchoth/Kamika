from django.conf.urls import url, include
from django.contrib import admin
from idream import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^academic', views.academic),
    url(r'^project-dream', views.project_dream),
    url(r'^medias', views.dream_media),
    url(r'^sponsor', views.sponsor),
    url(r'^album-images$', views.get_album_images)
]

"""
WSGI config for Kamika project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os, sys
#sys.path.append('/home/deploy/kamyka_env/lib/python2.7/site-packages')
print(sys.path)
from django.core.wsgi import get_wsgi_application
sys.path.append('/home/deploy/kamyka_env/Kamika')
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "Kamika.settings")

application = get_wsgi_application()

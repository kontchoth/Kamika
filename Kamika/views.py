from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render, redirect
from django.core.mail import send_mail

def default_page(request):
	return redirect('/icoach')
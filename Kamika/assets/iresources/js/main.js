var current = null;
$(document).ready(function() {
    set_height();

    // run test on resize of the window
    $(window).resize(set_height);

    // activate nav
   	$('.nav li.dropdown>a').click(function(){
   		$(this).parent('li').addClass('active');
   	});

    // activate nav
    $('.nav a').click(function(){
      $('.nav li').removeClass('current');
      $(this).parent('li').addClass('current');
    });

   	//active correct link in nav
   	var hash = window.location.hash;
   	var atag = $('.nav a[href="'+hash+'"]').parent('li');
   	$(atag).addClass('current');
   	if ($(atag).hasClass('dropdown')){
   		$(atag).addClass('active');
   	}
    display_content();
});

function display_content(){
  var hash = window.location.hash;
  if (current == null){
    $(hash).addClass('visible');
    current = hash;
  } else{
    $(current).removeClass('visible')
    $(hash).addClass('visible');
    current = hash;
  }
  set_height();
}

function set_height(){
  var height = $('#main').outerHeight();  
	$('#fullh').height(height);
}
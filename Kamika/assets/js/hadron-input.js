'use strict';
$.fn.clickOff = function(callback, selfDestroy){
    var clicked = false;
    var parent = this;
    var destroy = selfDestroy || true;

    parent.click(function(){
        clicked = true;
    });

    $(document).click(function(event){
        if (!clicked){
            callback(parent, event);
        }

        if (destroy){

        }

        clicked = false;
    });
};

$(function(){
	var value = $('.hadron-input-container>input').val();
	if (value){
		value = value.trim();
	} else {
		value = '';
	}

	if(value.length == 0){
		$('.hadron-input-container>label').addClass('empty');
	} else {
		$('.hadron-input-container>label').removeClass('empty');
	}

	$('.hadron-input-container>select').parent().find('label').removeClass('empty');
		

	$('.hadron-input-container>input').on('change', function(event){
		var val = $(this).val().trim();
		if (val.length == 0){
			if($(this).parent().find('input').attr('required')){
				$(this).addClass('input-error');
			}
			
			$(this).val('');
			$(this).parent().find('label').addClass('empty');
		} else {
			$(this).removeClass('input-error');
			$(this).parent().find('label').removeClass('empty');
		}
	});
	$('.hadron-input-container>input').on('focus', function(event){
		$(this).parent().find('label').removeClass('empty');
	});
	$('.hadron-input-container>input').on('focusout', function(event){
		$(this).trigger('change');
	});

	$('.hadron-input-container>textarea').on('change', function(event){
		var val = $(this).val().trim();
		if (val.length == 0){
			if($(this).parent().find('textarea').attr('required')){
				$(this).addClass('input-error');
			}
			
			$(this).val('');
			$(this).parent().find('label').addClass('empty');
		} else {
			$(this).removeClass('input-error');
			$(this).parent().find('label').removeClass('empty');
		}
	});
	$('.hadron-input-container>textarea').on('focus', function(event){
		$(this).parent().find('label').removeClass('empty');
	});
	$('.hadron-input-container>textarea').on('focusout', function(event){
		$(this).trigger('change');
	});
	

	// cellphone
	var phones = [{ "mask": "(###) ###-####"}, { "mask": "(###) ###-##############"}];
	var phoneids = ['#cellphone', '#homephone'];
	phoneids.forEach(function(id){
		$(id).inputmask({ 
	        mask: phones, 
	        greedy: false, 
	        definitions: { '#': { validator: "[0-9]", cardinality: 1}} 
	    });	
	});

	var dates = [{"mask":"##/##/####"}];
	var dateids = ['#dob'];
	dateids.forEach(function(id){
		$(id).inputmask({
			mask: dates,
			greedy: false,
			definitions:{'#': {validator: "[0-9]", cardinality: 1}}
		});
	});
    

	$('.search-wrapper').click(function(event){
		event.stopPropagation();
	});

	$('.search-wrapper').clickOff(function(){
		$('.search-wrapper').removeClass('active');
	});
	

	$(document).ready(function () {
    var url = window.location;
    var a = url.pathname.split('/').slice(0, 2).join('/');
    $('.hnav a[href="'+ a +'"]').parent().addClass('active');
    $('.hnav a').filter(function() {
         return this.href == url;
    }).parent().addClass('active');
  });

  $(window).scroll(function() {
    var e = $(this).scrollTop();
    if (e > 80) {
      $('.search-bar').hide();
      $('#header').addClass('static');
    } else {
      $('.search-bar').show();
      $('#header').removeClass('static');
    }
  });
});


function navigation_manager(){
	// this function toggle the active link of the nav bar
	// to activate the correct link
	var url = window.location.pathname;
	if (url.substr(url.length - 1) == '/'){
		url = url.slice(0, -1);
	}
	var link = $('#icoach-nav a[href$="' + url + '"]');
	$($(link).parent()).addClass('active');

	$('#hamburger').click(function(){
		$(this).toggleClass('open');
		if($(this).hasClass('open')){
			$('#icoach-nav>ul').slideDown(300);
			$(this).addClass('ham-top');
		} else {
			$('#icoach-nav>ul').slideUp(300).delay(300).queue(function(next){
				$(this).removeClass('ham-top');
				next();
			});
			
		}
	});

	// make sure that the navigation bar is still present on resize
	// because toggle it on and off and small screen and putting it back
	// on large screen when the nav is off will force the nav to reappear
	// accordingly. The css will take care of rearanging it
	$(window).resize(function(){
		if($(this).width() >850){
			$('#icoach-nav>ul').show();
		} else {
			$('#icoach-nav>ul').hide(300);
			$('#hamburger').removeClass('ham-top');
			$('#hamburger').removeClass('open');
		}
	});

	// Making sure that the header hang at the top of the page while
	// scrolling down and get release when scrolling up pass the header
	$(window).scroll(function(){
		var navbar = $('#icoach-nav');
		if ($(this).scrollTop() > 210){
			$(navbar).addClass('fixed');
		} else {
			$(navbar).removeClass('fixed');
		}
	});
}

function link_effect(el, effect){
	var services = $(el);
	for (var i = 0; i < services.length; i++){
		$(services[i]).hover(function(){
			$(this).addClass(effect);
		}, function(){
			// $(this).removeClass(effect);
		});
	}
}

function clear_comment_form(){
	$('#author').val('');
	$('#email').val('');
	$('#website').val('');
	$('#comment').val('');
	$('#city').val('');
	$('#state').val('');
}

function post_comment(category, title){
	var author = $('#author').val(),
		email = $('#email').val(),
		website = $('#website').val(),
		comment = $('#comment').val(),
		follow_up = $('#follow-up').is(':checked'),
		blog_subs = $('#blog-subscription').is(':checked');
		message = '';
	if (author == ''){
		message += '<p class="error">Author is required</p>';
	}
	if (email == ''){
		message += '<p class="error">Email is required</p>';
	}
	if (website == ''){
		message += '<p class="error">Wesite is required</p>';
	}

	if (message != ''){
		$('.message-error').html(message);
		return;
	} else {
		$('.message-error').html('');
	}

	$.ajax({
		type: 'POST',
		url: '/icoach/blogs/post-comment/',
		data: {
			csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
    		author: author,
    		email: email,
    		website: website,
    		comment: comment,
    		blogname: title,
    		category: category,
    		follow_up: follow_up,
    		blog_subs: blog_subs
		},
		success: function(data){
			if (data.html){
				$('#more-comment').append(data.html);
				var num_comments = $('.cmt-wrapper>h3>span').html();
				$('.cmt-wrapper>h3>span').html(parseInt(num_comments) + 1);
				clear_comment_form();
			} else {
				$('.message-error').html('An error has occured while posting your comment. Please try again');
			}
			
		},
		dataType: 'json',
		error: function(xhr, errmsg, err){
			$('.message-error').html('An error has occured while posting your comment. Please try again');
		}
	});
}

function comment_form_navigation(){
	$('#cf').validate({
		rules: {
			author: {
				required: true,
				minlength: 2,
			},
			email: {
				required: true,
				email: true,
			},
			website: {
				required: true,
				minlength: 5
			},
			comment: {
				required: true,
				minlength: 2
			}
		}
	});
}

function post_review(){
	var author = $('#author').val(),
		email = $('#email').val(),
		comment = $('#comment').val(),
		city = $('#city').val(),
		state = $('#state').val(),
		message = '';
	if (author == ''){
		message += '<p class="error">Author is required</p>';
	}
	if (email == ''){
		message += '<p class="error">Email is required</p>';
	}
	
	if (message != ''){
		$('.message-error').html(message);
		return;
	} else {
		$('.message-error').html('');
	}

	$.ajax({
		type: 'POST',
		url: '/icoach/reviews/post-review/',
		data: {
			csrfmiddlewaretoken: document.getElementsByName('csrfmiddlewaretoken')[0].value,
    		author: author,
    		email: email,
    		comment: comment,
    		city: city,
    		state: state
		},
		success: function(data){
			if (data.html){
				$('#more-reviews').append(data.html);
				clear_comment_form();
			} else {
				$('.message-error').html('An error has occured while posting your comment. Please try again');
			}
			
		},
		dataType: 'json',
		error: function(xhr, errmsg, err){
			$('.message-error').html('An error has occured while posting your comment. Please try again');
		}
	});
}

function toggle_body(id){
	$(id).toggleClass('visible');
}

function close_modal(id){
	var modal = document.getElementById(id).style.display = 'none';
}

function evaluate_result(id){
	var form = $(id).find('form li');
	var score = 0;
	for (var i=0; i < form.length; i++){
		score += parseInt($(form[i]).find('select').val());
	}
	var modal = document.getElementById('self-evaluation-modal');
	var result = '';
	if (score <= 30){
		result = 'Life Coaching is not for you right now.';
	} else if (score <=60){
		result = 'Life Coaching could help you to look at your life from a ';
		result += 'different viewpoint as well as help you develop a plan to ';
		result += 'change what it is that you would like to change. However, ';
		result += 'if you decide to work with a life coach now, you should decide ';
		result += 'and commit that you will take the necessary action for your benefit, ';
		result += 'or you will not make lasting life-changing improvements.';
	} else {
		result = 'Congratulations! You are ready for a Life Coach! You are willing ';
		result += 'to do whatever it takes to create the life you deserve and desire.';
	}
	document.getElementById('self-evaluation-result').innerHTML = result;
	modal.style.display = 'block';
	
}

function extract_row_information(row){
	var cols = $(row).find('th');
	console.log(cols);
}

function submit_form(form){
	var user_response = [];
	var form_id = '#'+form;
	if (form_id == '#workshop-evaluation'){
		var form_table_rows = $('form'+form_id).find('tr');
		for (var i=1; i < form_table_rows.length; i++){
			var question = $(form_table_rows[i]).find('th.icoach-table-headers').html();
			var val = $(form_table_rows[i]).find('input[name=workshop-evaluation'+i+']:checked').val();
			var comment = $(form_table_rows[i]).find('input[type=text]').val();
			var temp = {question: question, answer: val, comment: comment};
			user_response.push(temp);
		}

		var form_inputs = $('form'+form_id).find('.hadron-input-container');
		for (var i=0; i < form_inputs.length; i++){
			var label = $(form_inputs[i]).find('label').html();
			var answer = $(form_inputs[i]).find('textarea').val();
			var temp = {question: label, answer: answer};
			user_response.push(temp);
		}
		var columns = ['Question', 'Answer', 'Comments'], rows = [];
		for (var i=0; i < user_response.length; i++){
			rows.push([user_response[i].question, user_response[i].answer, user_response[i].comment]);
		}

		var doc = new jsPDF('p', 'pt');
		doc.autoTable(columns, rows);
		// doc.save(form + '.pdf');
		doc.output("dataurlnewwindow");
		return true;
	} else if (form_id == '#potential'){
		var form = $('form#potential-form');
		var temp = $(form).find('input#client').val();
		user_response.push({client: temp});

		var list_questions = $(form).find('p#potential');
		for (var i=0; i < list_questions.length; i++){
			console.log(list_questions[i]);
			var question = $(list_questions[i]).html();
			var answer = $(list_questions[i]).find('input[name=e' + i+1 + ']:checked').val();
			user_response.push({question:question, answer:answer});
		}
		console.log(list_questions);
	}
	// var doc = new jsPDF();          
	// var elementHandler = {
	//   form_id: function (element, renderer) {
	//     return true;
	//   }
	// };
	// var source = window.document.getElementById(form);
	// console.log(source);
	// doc.fromHTML(source,10,10,{'width': 180,'elementHandlers': elementHandler});

	// doc.output("dataurlnewwindow");
	// var columns = ['Question', 'Answer', 'Comments'], rows = [];
	// for (var i=0; i < user_response.length; i++){
	// 	rows.push([user_response[i].question, user_response[i].answer, user_response[i].comment]);
	// }

	// var doc = new jsPDF('p', 'pt');
	// doc.autoTable(columns, rows);
	// // doc.save(form + '.pdf');
	// doc.output("dataurlnewwindow");
}

function display_album(title){
	console.log(title);
	$.ajax({
		type: 'GET',
		url: '/idream/album-images?album=' + title,
		success: function(data){
			var html = '<div id="gallery"><div class="inner"><ul>';
			data.forEach(function(image){
				html += '<li><img src="'+image+'"/></li>';
			});
			html += '</ul></div></div></div>';
			$('.gallery-wrapper').html(html);
			$("#gallery").click(function(){
				$('.gallery-wrapper').html('');
			});

			$('#gallery .inner').click(function(event){
				event.stopPropagation();
				return false;
			});
		},
		dataType: 'json',
		error: function(xhr, errmsg, err){
			$('.message-error').html('An error has occured while posting your comment. Please try again');
		}
	});
}

tinyMCE.init({
  selector: 'textarea',
  height: 500,
  theme: 'modern',
  plugins: "spellchecker,directionality,paste,searchreplace",
  directionality: "{{ directionality }}",
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
 console.log('Loaded');
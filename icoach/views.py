from django.http import HttpResponse
from django.template import RequestContext
from django.core.context_processors import csrf
from django.shortcuts import render_to_response, render
from django.core.mail import send_mail
from django.utils.safestring import mark_safe
from blog.Calendar import WorkoutCalendar
import datetime, json
from django.template.loader import render_to_string
from django_gravatar.helpers import get_gravatar_url, has_gravatar, get_gravatar_profile_url, calculate_gravatar_hash
from .models import Review
from .helpers import get_publications

def index(request):
	now = datetime.datetime.now()
	params = {
		'title': 'Home',
		'tiles': [
			{
				'title':'Blog',
				'description':"Looking for tips, a fresh perspective, or inspiration then follow our blogs as we reflect on various experiences and journey's in life.",
				'url':'/icoach/blogs'
			},
			{
				'title':'EmpoweringU',
				'description':'Looking for ways to build and inspire you or your organization? Check out the programs we offer.',
				'url':'/icoach/empowering-you'
			},
			{
				'title':'Consulting',
				'description':"Seeking guidance and advice as you navigate through your child's educational journey? Explore the services and resources that we offers.",
				'url':'/icoach/educational-consulting'
			}
		],
		'my_cal': mark_safe(WorkoutCalendar([]).formatmonth(now.year, now.month)),
		'show_idream': True
	}

	return render_to_response('icoach-index.html', params)

def empowering_you(request):
	now = datetime.datetime.now()
	params = {
		'title': 'Empowering You',
		'my_cal': mark_safe(WorkoutCalendar([]).formatmonth(now.year, now.month)),
		'show_calendar': True,
		'show_idream': True
	}
	return render_to_response('icoach-empowering-you.html', params)

def blogs(request):
	params = {
		'title': 'Blog'
	}
	return render_to_response('icoach-blogs.html', params)

def shop(request):
	params = {
		'title': 'Shop'
	}
	return render_to_response('icoach-shop.html', params)

def connect(request):
	now = datetime.datetime.now()
	params = {
		'title': 'Connect',
		'my_cal': mark_safe(WorkoutCalendar([]).formatmonth(now.year, now.month)),
		'show_calendar': True,
		'show_idream': True,
		'personalInfos': [{
			'text':'Why do you want Coaching?',
			'id': 'q1'
		},{
			'text': 'What specific issues would you like to work on?' ,
			'id': 'q2'
		},{
			'text': 'Have you ever been coached before? If so, describe your experience with coaching.',
			'id': 'q3'
		},{
			'text':'Are you or have you ever been in counseling or therapy? If yes, please explain.' ,
			'id': 'q4'
		},{
			'text': 'Describe your spirituality. What is your relationship with God? In what ways do you sense God might be challenging you, nudging you, or trying to get your attention?',
			'id': 'q5'
		},{
			'text': 'What are the major things happening in your life right now?',
			'id': 'q6'
		},{
			'text': 'How would you like your life to be different one year from now?',
			'id': 'q7'
		},{
			'text': 'What is getting in the way of these changes or goals?',
			'id': 'q8'
		},{
			'text': "List three things you are procrastinating on in your life right now.",
			'id': 'q9'
		},{
			'text': 'If we worked together, in what ways might you undermine or sabotage me as your coach? How would I help you stop doing this?',
			'id': 'q10'
		},{
			'text': 'What are your insecurities about being coached?',
			'id': 'q11'
		},{
			'text': 'Please include any other comments you wish to add.',
			'id': 'q12'
		}],
		'potentials':[
			'Wants to change and grow',
			'Has taken efforts to change or grow within the past year',
			'Is willing to consider new assumptions, values, and behaviors',
			'Is not involved in counseling at present',
			'Gives no evidence of personal problems that could interfere with the coaching process',
			'Is willing, if it seems wise, to get more training, do reading, and engage in other activities that could bring change and growth',
			"Is willing to restructure one's life if necessary'"
			'Understands that coaching is not mentoring, advice giving, or counseling',
			'Is capable of thinking about the future',
			'Has goals that are not yet being reached',
			'Is willing to work with a coach in a collaborative relationship',
			'Is open to learning from others',
			'Appears willing and able to persist in moving toward goals',
			"Is open for God's leading in the coaching process",
			"Appears to be 'in sync' and have good chemistry with the coach"
		],
		'coachings':[
			'I am ready to create more balance in my life.',
			'I am ready to improve my personal or business relationships.',
			'I am ready to make real and positive changes in my life.',
			'I am ready to find and live my life\'s purpose.',
			'I am ready and willing to overcome self-limiting beliefs and behavior.',
			'I am ready to create plans and take action to achieve my goals.',
			'I am ready to achieve a sense of fulfillment at work and in my life.',
			'I am ready for more fun and enjoyment in my life.',
			'I\'d like to work less and make more money.',
			'I can benefit from someone who will help me to stay on track.'
		],
		'workshops':[
			'Presenter Overall',
			'Presentation:  How did the workshop meet your expectations?',
			'Materials/Handouts',
			'Assisted you with new information',
			'Information and skill building',
			'Responded to questions',
			'Overall Evaluation'
		],
		'evaluations':[
			'What did you most like about this workshop?',
			'What did you least like about this workshop?',
			'Is there anything else you would like the workshop to have covered?',
			'What would you like to see offered next time?',
			'Additional Comments/Recommendations:'
		]
	}
	return render_to_response('icoach-connect.html', params)

def rares_reviews(request):
	params = {
		'title': 'Rares & Reviews',
		'reviews': Review.objects.all()
	}
	return render_to_response('icoach-rares-reviews.html', params, RequestContext(request))

def educational_consulting(request):
	params = {
		'title': 'Educational Consulting',
		'publications': get_publications()
	}
	return render_to_response('icoach-educational-consulting.html', params)

def medias(request):
	params = {
		'title': 'Medias'
	}
	return render_to_response('icoach-medias.html', params)

def post_review(request):
	if request.method == 'POST':
		author = request.POST['author']
		email = request.POST['email']
		city = request.POST['city']
		state = request.POST['state']
		message = request.POST['comment']
		review = Review(author=author, email=email, city=city, state=state, message=message)
		review.save()
		html = render_to_string('partials/icoach-review-template.html', {'review': review})
		return HttpResponse(json.dumps({'html': html, 'status':200}), content_type='application/json')
	return HttpResponse(json.dumps({'status': 400}))
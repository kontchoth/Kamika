from django import template


register = template.Library()

@register.filter(name='times')
def times(num, start):
  return range(start, num+1)


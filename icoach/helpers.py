from icoach.models import Publication
from django.core.exceptions import *


def get_publications():
  return Publication.objects.all().filter(active=True).order_by('date')[::-1]  
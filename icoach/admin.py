from django.contrib import admin
from .models import Publication

class PublicationAdmin(admin.ModelAdmin):
  fieldsets = (
    ('Publication Information', {
      'fields': ('title', 'date', 'publisher', 'description','url', 'cover', 'active')
    }),
  )
  list_displat = ('title', 'publisher','date')

# Registering the model into the admin page
admin.site.register(Publication, PublicationAdmin)
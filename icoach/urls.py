from django.conf.urls import url, include
from django.contrib import admin
from icoach import views
import blog
import icoachresources

urlpatterns = [
    url(r'^$', views.index),
    url(r'^empowering-you$', views.empowering_you),
    url(r'^connect$', views.connect),
    url(r'^rares-reviews$', views.rares_reviews),
    url(r'^educational-consulting$', views.educational_consulting),
    url(r'^medias$', views.medias),
    url(r'^blogs/', include('blog.urls')),
    url(r'^reviews/post-review/$', views.post_review),
]

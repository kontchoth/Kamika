from __future__ import unicode_literals

from django.db import models
from datetime import datetime
import django


class Review(models.Model):
	author = models.CharField(max_length=200, blank=False, editable=False)
	active = models.BooleanField(default=True, blank=False)
	posted_date = models.DateTimeField(editable=False, default=django.utils.timezone.now)
	city = models.CharField(max_length=100, editable=False, blank=True, default='')
	state = models.CharField(max_length=100, editable=False, blank=True, default='')
	email = models.EmailField(editable=False, blank=True, default='', max_length=200)
	message = models.TextField(blank=False)

	def __str__(self):
		return self.author

class Publication(models.Model):
  publisher = models.CharField(max_length=200, blank=True, default='Not Avalaible')
  date = models.DateTimeField(editable=True, default=django.utils.timezone.now)
  title = models.CharField(max_length=300, default='Not Available', blank=False)
  description = models.TextField(blank=False)
  url = models.TextField(max_length=400, blank=True)
  cover = models.ImageField(upload_to='publication-covers', blank=False)
  active = models.BooleanField(default=True)
  def __str__(self):
    return self.title


